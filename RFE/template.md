# Enhancement/Feature description

This section is mandatory.

Please describe the feature you want to introduce or the use case you want to address. Don't hesitate to provide external links or diagrams so that everyone can understand what you're talking about.

# Technical implementation

This section is optional.

If you already have an idea of how to implement your proposal, what components could be used to cover the use case, don't hesitate to provide details in this section.

# How to test it

This section is optional.

We need to identify a way to test the feature, with test cases that can be run in the CI, to ensure the feature keeps running while the Sylva stack evolve.
